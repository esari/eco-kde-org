---
title: Get Involved
subtitle: Become a part of the sustainable software movement! 
name: KDE Eco
userbase: KDE Eco
---
We need as many motivated people as possible to drive this forward. Here are some channels where you can get more information and contribute.

### Community & Support

- Monthly Meet-up (Video Conference): 2nd Wednesdays 19:00 CEST/CET (UTC+2:00/+1:00) (contact us for details)
- Matrix Room (Discussion): https://webchat.kde.org/#/room/#kde-eco:kde.org
- Mailing List (Discussion): https://mail.kde.org/cgi-bin/mailman/listinfo/kde-eco-discuss
- Mailing List (Announcements): https://mail.kde.org/cgi-bin/mailman/listinfo/kde-eco-announce

### Resources

- Sustainable Software Goal (Community Wiki): https://community.kde.org/Goals/Sustainable_Software
- "Opt Green" project (GitLab Repository): https://invent.kde.org/teams/eco/opt-green
- Remote Eco Lab (GitLab Repository): https://invent.kde.org/teams/eco/remote-eco-lab
- FEEP (GitLab Repository): https://invent.kde.org/teams/eco/feep
- BE4FOSS (GitLab Repository): https://invent.kde.org/teams/eco/be4foss
- Blue Angel Applications (GitLab Repository): https://invent.kde.org/teams/eco/blue-angel-application

### Contact

Email: `joseph [at] kde.org`
