---
title: Green Web
layout: greenweb
#menu:
#   main:
#     name: greenweb
#     weight: 3
---
This website was checked by the [greenwebfoundation.org](https://www.thegreenwebfoundation.org/green-web-check/?url=https%3A%2F%2Feco.kde.org) and is labelled “Hosted Green”.

Website is hosted by: Hetzner Online AG. Supporting evidence for this hoster’s claims:

 - [Sustainability page](https://www.hetzner.com/unternehmen/umweltschutz/)
