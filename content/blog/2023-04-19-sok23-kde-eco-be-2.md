---
date: 2023-04-19
title: "Season Of KDE 2023: Wrapping Up My Work With KDE Eco"
categories: [SOK23, Blauer Engel, KdeEcoTest, GCompris, Kate]
author: Rudraksh Karpe
summary: This post wraps up my journey as a mentee in Season of KDE 2023. Here I discuss resolving various issues as well as my work expanding the tool set for energy consumption measurements of KDE applications.
SPDX-License-Identifier: CC-BY-SA-4.0
authors:
- SPDX-FileCopyrightText: 2023 Rudraksh Karpe <@rudrakshkarpe:matrix.org>
---

This post wraps up my journey as a mentee in Season of KDE 2023, where I contributed to the preparation of Standard Usage Scenarios for the KDE applications [GCompris](https://apps.kde.org/gcompris/) and [Kate](https://apps.kde.org/kate/) while also working on a new tool to automate the formatting of log files.

From troubleshooting screen resolution issues to creating a bash script for resetting user configurations, I hope this blog post provides the reader with useful insights and solutions. First, I share experiences with screen resolution problems when sharing Standard Usage Scenarios (SUS) across reference systems—and one way to overcome them. I also present a step-by-step guide for using KdeEcoTest with GCompris, and provide details about collaborative work porting the existing `xdotool`-based [Kate](https://invent.kde.org/teams/eco/feep/-/blob/master/usage_scenarios/kate/kate.sh) script with KdeEcoTest. Finally, I give a progress update for the KDE-SUS Log Formatter, a tool used for formatting log files of actions. 

### Resolving Screen Resolution Issues

As discussed in [my earlier blog post](https://eco.kde.org/blog/2023-03-15-sok23-kde-eco_be/), there were issues when generating a Standard Usage Scenario for GCompris using [KdeEcoTest](https://invent.kde.org/teams/eco/feep/-/tree/master/tools/KdeEcoTest). The main challenge was as follows: when the GCompris script was shared between two machines with different screen resolutions, the resulting UI changes made the script invalid. Specifically, GCompris uses the primary screen resolution to calculate icon size, which led to the script failing to run on a machine with a different resolution.

To address this issue, SoK23 mentee Mohamed Ibrahim (from the KdeEcoTest project) and I used virtual machines configured with the same screen resolution. We generated a GCompris script on a machine with a screen resolution of 1366X768 and then tested it on a machine with a screen resolution of 1920x1080. However, the script failed to run due to the resulting UI changes. After further troubleshooting, we were able to fix this by [generating a script](https://invent.kde.org/teams/eco/feep/-/blob/master/tools/KdeEcoTest/tests/Gcompris_590.txt) where the GCompris window was kept to around 590x590 pixels as a modal window.

We also encountered issues with the [original GCompris script](https://invent.kde.org/teams/eco/feep/-/blob/master/tools/KdeEcoTest/tests/KdeEcoGComprisTestScript.txt) written by [Emmanuel Charruau](https://invent.kde.org/echarruau), as it was not working as expected. This was due to the script resizing the GCompris window to around 830x830 pixels in the modal window, reaching the maximum height of the screen and resulting in the window being maximized. To fix this, we kept the GCompris window to around 590x590 pixels as a modal window, ensuring that the script would run as expected when the screen resolution remained the same.

{{< video src="/blog/videos/GCompris-using-KdeEcoTest-590x590.mp4" controls=true autoplay=false loop=false >}}

{{< container class="text-center" >}}
Video: Running GCompris with KdeEcoTest with a 590x590 screen resolution. (Video from Rudraksh Karpe published under a [CC-BY-SA-4.0](https://spdx.org/licenses/CC-BY-SA-4.0.html) license.)
{{< /container >}}

### Using KdeEcoTest With GCompris And Kate, And Resetting User Configurations

I had the opportunity to explore different aspects of GCompris and contributed to creating a [step-by-step guide](https://invent.kde.org/teams/eco/feep/-/merge_requests/39) to using KdeEcoTest with GCompris. This guide covers the use of virtual machines to maintain consistent screen resolutions and tips for avoiding UI issues by keeping the GCompris window a specific size. Through this task, I gained valuable experience in troubleshooting issues and learned how to write concise and effective documentation.

Additionally, I also generated a [bash script](https://invent.kde.org/teams/eco/feep/-/merge_requests/41/diffs?commit_id=6745e4163e721e049f6fc2bab647b6518817f1ae) to reset GCompris's configurations. This script enables users to quickly reset their GCompris settings to their default configuration, which can be a useful troubleshooting step while generating a SUS.

In collaboration with Mohamed, I additionally worked on generating a [bash script](https://invent.kde.org/teams/eco/feep/-/merge_requests/41/diffs?commit_id=ea6901f2dc681eb5e79adcd894c762986df9de2a) to reset Kate's configurations. Moreover, previously `xdotool` was used for generating the Kate SUS to meet the Blue Angel criteria, but we aimed to maintain consistency in generating scripts. Hence, we worked on porting the [Kate script to KdeEcoTest](https://invent.kde.org/teams/eco/feep/-/merge_requests/41/diffs?commit_id=24d87232f87af62f9958aa1daff7f8a08a9465ae).

{{< video src="/blog/videos/Kate-script-KdeEcoTest.webm" controls=true autoplay=false loop=false >}}

{{< container class="text-center" >}}
Video: Running the Kate script with KdeEcoTest. (Video from Rudraksh Karpe published under a [CC-BY-SA-4.0](https://spdx.org/licenses/CC-BY-SA-4.0.html) license.)
{{< /container >}}

### Progress With KDE-SUS Log Formatter

As also mentioned in my previous blog post, I have been working on the [KDE-SUS Log Formatter](https://invent.kde.org/teams/eco/feep/-/merge_requests/30) as an experimental part of my Season of KDE journey. The KDE-SUS Log Formatter is a tool that automates the process of determining the elapsed time for Standard Usage Scenarios (SUS) action logs generated by `KdeEcoTest` or `xdotool`. Throughout the season, I added several features to the tool, including:

- Formatting logs generated by `KdeEcoTest` or `xdotool`
- Supporting the CSV format (with an ongoing parquet branch that supports Parquet format)
- Displaying formatted logs using a command line interface (CLI)
- Saving formatted logs in a new file
- Using a terminal-based user interface (UI)

I also had the opportunity to present my work on KDE-SUS Log Formatter at the monthly KDE Eco online meetup.

{{< video src="/blog/videos/KDE-Log-Formatter-Demo.webm" controls=true autoplay=false loop=false >}}

{{< container class="text-center" >}}
Video: Demonstrating KDE-SUS Log Formatter. (Video from Rudraksh Karpe published under a CC-BY-SA-4.0 license.)
{{< /container >}}


### Conclusion

I would like to express gratitude to my mentor [Joseph P. De Veaugh-Geiss](https://invent.kde.org/joseph), who provided invaluable guidance and taught me about the values and ethics of Free & Open Source Software during my participation in the Season of KDE program. I also extend a heartfelt thank you to [Emmanuel Charruau](https://invent.kde.org/echarruau) and [Karanjot Singh](https://invent.kde.org/drquark), mentors from the collaborating KdeEcoTest project, for their ongoing support and guidance throughout SoK23. [Mohamed Ibrahim](https://invent.kde.org/hemasonus), a fellow SoK mentee, was instrumental in helping me understand KdeEcoTest and its inner workings. I am thankful for his collaboration. I would also like to thank the KDE Eco community for their encouragement and support.
